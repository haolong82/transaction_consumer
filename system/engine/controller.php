<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* Controller class
*/
abstract class Controller {
	protected $registry;
	public $statusCode = 200;
    public $json = array("success" => 1, "error" => array(), "data" => array());
    public $allowedHeaders = array("GET", "POST", "PUT", "DELETE");
    private $httpVersion = "HTTP/1.1";

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}

	public function sendResponse() {
        $statusMessage = $this->getHttpStatusMessage($this->statusCode);

        if ($this->statusCode != 200) {
            if (!isset($this->json["error"])) {
                $this->json["error"][] = $statusMessage;
            }

            if ($this->statusCode == 405) {
                header('Allow: '.implode(",", $this->allowedHeaders));
            }

            $this->json["success"] = 0;
        }

        header($this->httpVersion . " " . $this->statusCode . " " . $statusMessage);
        header('Content-Type: application/json; charset=utf-8');

        if(defined('JSON_UNESCAPED_UNICODE')){
            echo(json_encode($this->json, JSON_UNESCAPED_UNICODE));
        } else {
            echo $this->rawJsonEncode($this->json);
        }
        die;
    }

    public function getHttpStatusMessage($statusCode) {
        $httpStatus = array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed'
        );

        return ($httpStatus[$statusCode]) ? $httpStatus[$statusCode] : $httpStatus[500];
    }

    public function getPost() {
        $input = file_get_contents('php://input');
        $post = json_decode($input, true);

        if (!is_array($post) || empty($post)) {
            $this->statusCode = 400;
            $this->json['error'][] = 'Invalid request body, please validate the json object';
            return $this->sendResponse();
        }

        return $post;
    }

    public function loadData(&$post, $default_fields, $default_field_values, $current_data = null) {
        foreach ($default_fields as $field) {
            if (!isset($post[$field])) {
                if (!empty($current_data) && isset($current_data[$field])) {
                    $post[$field] = $current_data[$field];
                } else {
                    $post[$field] = (isset($default_field_values[$field]) && empty($current_data) ) ? $default_field_values[$field] : "";
                }
            }
        }
    }
}
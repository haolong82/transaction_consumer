<?php
class ControllerTransactionTransaction extends Controller {
	static $default_fields = array(
		'reporting_partner_id',
		'reporting_consumer_id',
        'offer_id',
        'consumer',
        'offer_location_id',
        'datetime_reported',
        'transaction_type',
        'metrics',
        'meta_data',
		'version'
    );

    static $default_field_values = array(
		'reporting_partner_id' => '',
		'reporting_consumer_id' => '',
        'offer_id' => '',
        'consumer' => [],
        'offer_location_id' => '',
        'transaction_type' => '',
        'metrics' => '',
        'meta_data' => '',
		'version' => ''
    );

    static $consumer_fields = array(
		'reporting_partner_id',
		'reporting_consumer_id',
		'primary_mobile_number',
		'primary_payment_method_id',
		'aws_cognito_id',
		'consumer_status',
		'auth_token',
		'account_type',
		'mobile_number',
		'first_name',
		'last_name',
		'gender',
		'birthday',
		'address',
		'salary',
		'account_status'
    );

    static $consumer_field_values = array(
    	'reporting_partner_id' => '',
		'reporting_consumer_id' => '',
		'primary_mobile_number' => '',
		'primary_payment_method_id' => 0,
		'aws_cognito_id' => 0,
		'consumer_status' => 'active',
		'auth_token' => '',
		'account_type' => '',
		'mobile_number' => '',
		'first_name' => '',
		'last_name' => '',
		'gender' => '',
		'birthday' => '',
		'address' => '',
		'salary' => 0,
		'account_status' => 'active'
    );

    static $default_status = array(
    	'active', 
    	'disabled', 
    	'blacklisted', 
    	'delete'
    );

	public function index() {
        if ($this->request->server['REQUEST_METHOD'] === 'GET') {
            $this->get();
        } else if ($this->request->server['REQUEST_METHOD'] === 'POST') {
            $this->add();
        } else if ($this->request->server['REQUEST_METHOD'] === 'PUT') {
            $this->edit();
        } else if ($this->request->server['REQUEST_METHOD'] === 'DELETE') {
			$this->delete();
        }

        return $this->sendResponse();
    }

	public function get() {
		$this->load->language('transaction/transaction');

		$this->load->model('transaction/transaction');

		if (isset($this->request->get['filter_transaction_id'])) {
			$filter_transaction_id = $this->request->get['filter_transaction_id'];
		} else {
			$filter_transaction_id = '';
		}

		if (isset($this->request->get['filter_reporting_partner_id'])) {
			$filter_reporting_partner_id = $this->request->get['filter_reporting_partner_id'];
		} else {
			$filter_reporting_partner_id = '';
		}

		if (isset($this->request->get['filter_reporting_consumer_id'])) {
			$filter_reporting_consumer_id = $this->request->get['filter_reporting_consumer_id'];
		} else {
			$filter_reporting_consumer_id = '';
		}

		if (isset($this->request->get['filter_offer_id'])) {
			$filter_offer_id = $this->request->get['filter_offer_id'];
		} else {
			$filter_offer_id = '';
		}

		if (isset($this->request->get['filter_consumer_id'])) {
			$filter_consumer_id = $this->request->get['filter_consumer_id'];
		} else {
			$filter_consumer_id = '';
		}

		if (isset($this->request->get['filter_offer_location_id'])) {
			$filter_offer_location_id = $this->request->get['filter_offer_location_id'];
		} else {
			$filter_offer_location_id = '';
		}

		if (isset($this->request->get['filter_datetime_reported'])) {
			$filter_datetime_reported = $this->request->get['filter_datetime_reported'];
		} else {
			$filter_datetime_reported = '';
		}

		if (isset($this->request->get['filter_datetime_created'])) {
			$filter_datetime_created = $this->request->get['filter_datetime_created'];
		} else {
			$filter_datetime_created = '';
		}

		if (isset($this->request->get['filter_datetime_modified'])) {
			$filter_datetime_modified = $this->request->get['filter_datetime_modified'];
		} else {
			$filter_datetime_modified = '';
		}

		if (isset($this->request->get['filter_transaction_type'])) {
			$filter_transaction_type = $this->request->get['filter_transaction_type'];
		} else {
			$filter_transaction_type = '';
		}

		if (isset($this->request->get['filter_version'])) {
			$filter_version = $this->request->get['filter_version'];
		} else {
			$filter_version = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'transaction_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_transaction_id'        => $filter_transaction_id,
			'filter_reporting_partner_id'	     => $filter_reporting_partner_id,
			'filter_reporting_consumer_id'	     => $filter_reporting_consumer_id,
			'filter_offer_id'    => $filter_offer_id,
			'filter_consumer_id' => $filter_consumer_id,
			'filter_offer_location_id'           => $filter_offer_location_id,
			'filter_datetime_reported'      => $filter_datetime_reported,
			'filter_datetime_created'   => $filter_datetime_created,
			'filter_datetime_modified'   => $filter_datetime_modified,
			'filter_transaction_type'   => $filter_transaction_type,
			'filter_version'   => $filter_version,
			'sort'                   => $sort,
			'order'                  => $order,
			'start'                  => ($page - 1) * 50,
			'limit'                  => 50
			);

		$results = $this->model_transaction_transaction->getTransactions($filter_data);

		$this->json['data'] = json_encode($results);
		
		return $this->sendResponse();
	}

	public function add() {
		$this->load->language('transaction/transaction');

		$this->load->model('transaction/transaction');
		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->getPost();

        	$this->loadData($post, self::$default_fields, self::$default_field_values);

	        $error = $this->validateForm($post);

	        if (empty($error)) {
	        	$post['consumer']['reporting_partner_id'] = $post['reporting_partner_id']; 
	        	$post['consumer']['reporting_consumer_id'] = $post['reporting_consumer_id'];

	        	$this->loadData($post['consumer'], self::$consumer_fields, self::$consumer_field_values);

	        	$consumer_id = $this->model_consumer_consumer->existConsumer($post['consumer']);

	        	if(!$consumer_id) {
	        		$post['consumer_id'] = $this->model_consumer_consumer->addConsumer($post['consumer']);

	        		$post['consumer']['consumer_id'] = $post['consumer_id'];

	        		$this->model_consumer_consumer->addThirdParty($post['consumer']);
	        	} else {
	        		$this->model_consumer_consumer->editConsumer($consumer_id, $post['consumer']);

	        		$this->model_consumer_consumer->editThirdParty($consumer_id, $post['consumer']);

	        		$post['consumer_id'] = $consumer_id;
	        	}
				$this->json['data']['transaction_id'] = $this->model_transaction_transaction->addTransaction($post);
			} else {
	            $this->json['error'] = $error;
	            $this->statusCode = 400;
	        }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}
		
		return $this->sendResponse();
	}

	public function edit() {
		$this->load->language('transaction/transaction');

		$this->load->model('transaction/transaction');

		if ($this->request->server['REQUEST_METHOD'] == 'PUT') {
			$post = $this->getPost();

        	if (isset($this->request->get['transaction_id']) && $this->request->get['transaction_id'] && !empty($post)) {
        		$transaction = $this->model_transaction_transaction->getTransaction($this->request->get['transaction_id']);

		        if($transaction) {
		            $this->loadData($post, self::$default_fields, self::$default_field_values, $transaction);

		            $error = $this->validateForm($post, $this->request->get['transaction_id']);

            		if (!empty($post) && empty($error)) {
						$this->model_transaction_transaction->editTransaction($this->request->get['transaction_id'], $post);
						$this->json['data'] = $this->model_transaction_transaction->getTransaction($this->request->get['transaction_id']);
					} else {
		                $this->json['error'] = $error;
		                $this->statusCode = 400;
		            }
				} else {
	                $this->json['error'][] = "Transaction not found";
	                $this->statusCode = 404;
	            }
			} else {
                $this->statusCode = 400;
                $this->json['error'][] = "No transaction ID or input data";
            }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}

		return $this->sendResponse();
	}

	public function delete() {
		$this->load->language('transaction/transaction');

		$this->load->model('transaction/transaction');

		if($this->request->server['REQUEST_METHOD'] == 'DELETE') {
			if(isset($this->request->get['transaction_id']) && $this->request->get['transaction_id']) {
        		$transaction = $this->model_transaction_transaction->getTransaction($this->request->get['transaction_id']);

		        if($transaction) {
					$this->model_transaction_transaction->deleteTransactionByTransactionId($this->request->get['transaction_id']);
				} else {
	                $this->json['error'] = "Transaction not found";
	                $this->statusCode = 404;
	            }
			} else {
				$this->json['error'][] = 'Error transaction ID';
                $this->statusCode = 400;
			}
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}

		return $this->sendResponse();
	}

	private function validateForm($post, $transaction_id = null) {
        $error = array();

        if (!$post['reporting_partner_id']) {
            $error[] = 'No reporting partner ID';
        }

        if (!$post['reporting_consumer_id']) {
            $error[] = 'No reporting consumer ID';
        }

        if (!$post['offer_id']) {
            $error[] = 'No offer ID';
        }

        if (empty($post['consumer']) || !is_array($post['consumer'])) {
            $error[] = 'No consumer or consumer is not an object';
        }

        if (isset($post['consumer']['mobile_number']) && (utf8_strlen($post['consumer']['mobile_number']) < 7 || utf8_strlen($post['consumer']['mobile_number']) > 32)) {
            $error[] = 'Error mobile number';
        }

         if (!empty($post['consumer']['first_name']) && utf8_strlen($post['consumer']['first_name']) > 32) {
            $error[] = 'Error first name';
        }

        if (!empty($post['consumer']['last_name']) && utf8_strlen($post['consumer']['last_name']) > 32) {
            $error[] = 'Error last name';
        }

        if (!empty($post['consumer']['payment_method']) && (utf8_strlen($post['consumer']['payment_method']) < 3 || utf8_strlen($post['consumer']['payment_method']) > 128)) {
            $error[] = 'Error payment method';
        }

        if (!empty($post['consumer']['address']) && (utf8_strlen($post['consumer']['address']) < 2 || utf8_strlen($post['consumer']['address']) > 128)) {
            $error[] = 'Address length error(2-256)';
        }

        if (!empty($post['consumer']['birthday']) && (strtotime($post['consumer']['birthday']) < strtotime('1900-01-01') || strtotime($post['consumer']['birthday']) > strtotime('2010-01-01'))) {
            $error[] = 'Error birthday';
        }

        if (!empty($post['consumer']['consumer_status']) && !in_array($post['consumer']['consumer_status'], self::$default_status)) {
        	$error[] = 'Error consumer status';
        }

        return $error;
    }
}

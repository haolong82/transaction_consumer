<?php
class ControllerConsumerPaymentMethod extends Controller {
	static $default_fields = array(
		'payment_service_name',
		'service_user_identifier',
		'meta_data',
		'status'
    );

    static $default_field_values = array(
		'service_user_identifier' => '',
		'meta_data' => '',
		'status' => 'active'
    );

    static $default_status = array(
    	'active', 
    	'disabled', 
    	'blacklisted', 
    	'delete'
    );

	public function index() {
        if ($this->request->server['REQUEST_METHOD'] === 'GET') {
            $this->get();
        } else if ($this->request->server['REQUEST_METHOD'] === 'POST') {
            $this->add();
        } else if ($this->request->server['REQUEST_METHOD'] === 'PUT') {
            $this->edit();
        } else if ($this->request->server['REQUEST_METHOD'] === 'DELETE') {
			$this->delete();
        }

        return $this->sendResponse();
    }

	public function get() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if (isset($this->request->get['filter_payment_method_id'])) {
			$filter_payment_method_id = $this->request->get['filter_payment_method_id'];
		} else {
			$filter_payment_method_id = '';
		}

		if (isset($this->request->get['filter_payment_service_name'])) {
			$filter_payment_service_name = $this->request->get['filter_payment_service_name'];
		} else {
			$filter_payment_service_name = '';
		}

		if (isset($this->request->get['filter_service_user_identifier'])) {
			$filter_service_user_identifier = $this->request->get['filter_service_user_identifier'];
		} else {
			$filter_service_user_identifier = '';
		}

		if (isset($this->request->get['filter_meta_data'])) {
			$filter_meta_data = $this->request->get['filter_meta_data'];
		} else {
			$filter_meta_data = '';
		}

		if (isset($this->request->get['filter_status'])) {
			$status = $this->request->get['filter_status'];
		} else {
			$status = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'payment_method_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_payment_method_id'        => $filter_payment_method_id,
			'filter_payment_service_name'	  => $filter_payment_service_name,
			'filter_service_user_identifier'  => $filter_service_user_identifier,
			'filter_meta_data'           	  => $filter_meta_data,
			'filter_datetime_created'   	  => $filter_datetime_created,
			'filter_datetime_modified'   	  => $filter_datetime_modified,
			'filter_status'   				  => $filter_status,
			'sort'                   => $sort,
			'order'                  => $order,
			'start'                  => ($page - 1) * 50,
			'limit'                  => 50
			);

		$results = $this->model_consumer_consumer->getConsumers($filter_data);

		$this->json['data'] = json_encode($results);
		
		return $this->sendResponse();
	}


	public function add() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->getPost();

        	$this->loadData($post, self::$default_fields, self::$default_field_values);

	        $error = $this->validateForm($post);

	        if (empty($error)) {
				$this->json['data']['payment_method_id'] = $this->model_consumer_consumer->addPaymentMethod($post);
			} else {
	            $this->json['error'] = $error;
	            $this->statusCode = 400;
	        }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}
		
		return $this->sendResponse();
	}

	public function edit() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'PUT') {
			$post = $this->getPost();

        	if (isset($this->request->get['payment_method_id']) && ctype_digit($this->request->get['payment_method_id']) && !empty($post)) {
        		$payment_method = $this->model_consumer_consumer->getPaymentMethod($this->request->get['payment_method_id']);

		        if($payment_method) {
		            $this->loadData($post, self::$default_fields, self::$default_field_values, $payment_method);

		            $error = $this->validateForm($post, $this->request->get['payment_method_id']);

            		if (!empty($post) && empty($error)) {
						$this->model_consumer_consumer->editPaymentMethod($this->request->get['payment_method_id'], $post);
						$this->json['data'] = $this->model_consumer_consumer->getPaymentMethod($this->request->get['payment_method_id']);
					} else {
		                $this->json['error'] = $error;
		                $this->statusCode = 400;
		            }
				} else {
	                $this->json['error'][] = "PaymentMethod not found";
	                $this->statusCode = 404;
	            }
			} else {
                $this->statusCode = 400;
                $this->json['error'][] = "No PaymentMethod ID or Input Data";
            }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("PUT");
		}

		return $this->sendResponse();
	}

	public function delete() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if($this->request->server['REQUEST_METHOD'] == 'DELETE') {
        	if(isset($this->request->get['payment_method_id']) && ctype_digit($this->request->get['payment_method_id'])) {
        		$payment_method = $this->model_consumer_consumer->getPaymentMethod($this->request->get['payment_method_id']);

		        if($payment_method) {
					$this->model_consumer_consumer->deletePaymentMethod($this->request->get['payment_method_id']);
				} else {
	                $this->json['error'] = "Payment method not found";
	                $this->statusCode = 404;
	            }
			} else {
				$this->json['error'][] = 'Error payment method ID';
                $this->statusCode = 400;
			}
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("DELETE");
		}

		return $this->sendResponse();
	}

    private function validateForm($post, $payment_method_id = null) {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

        $error = array();

        if (!isset($post['payment_service_name']) || utf8_strlen($post['payment_service_name']) < 2 || utf8_strlen($post['payment_service_name']) > 32) {
            $error[] = 'Error payment service name';
        }

        if(isset($post['status']) && !in_array($post['status'], self::$default_status)) {
        	$error[] = 'Error status';
        }

        return $error;
    }
}

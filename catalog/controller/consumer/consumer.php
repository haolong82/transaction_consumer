<?php
class ControllerConsumerConsumer extends Controller {
	static $default_fields = array(
		'consumer_id',
		'reporting_partner_id',
		'reporting_consumer_id',
		'primary_mobile_number',
		'primary_payment_method_id',
		'aws_cognito_id',
		'consumer_status'
    );

    static $default_field_values = array(
    	'reporting_partner_id' => '',
		'reporting_consumer_id' => '',
		'primary_mobile_number' => '',
		'primary_payment_method_id' => 0,
		'aws_cognito_id' => '',
		'consumer_status' => 'active'
    );

    static $default_consumer_status = array(
    	'active', 
    	'disabled', 
    	'blacklisted', 
    	'delete'
    );

    public function index() {
        if ($this->request->server['REQUEST_METHOD'] === 'GET') {
            $this->get();
        } else if ($this->request->server['REQUEST_METHOD'] === 'POST') {
            $this->add();
        } else if ($this->request->server['REQUEST_METHOD'] === 'PUT') {
            $this->edit();
        } else if ($this->request->server['REQUEST_METHOD'] === 'DELETE') {
			$this->delete();
        }

        return $this->sendResponse();
    }

	public function get() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if (isset($this->request->get['filter_consumer_id'])) {
			$filter_consumer_id = $this->request->get['filter_consumer_id'];
		} else {
			$filter_consumer_id = '';
		}

		if (isset($this->request->get['filter_primary_mobile_number'])) {
			$filter_primary_mobile_number = $this->request->get['filter_primary_mobile_number'];
		} else {
			$filter_primary_mobile_number = '';
		}

		if (isset($this->request->get['filter_primary_payment_method_id'])) {
			$filter_primary_payment_method_id = $this->request->get['filter_primary_payment_method_id'];
		} else {
			$filter_primary_payment_method_id = 0;
		}

		if (isset($this->request->get['filter_datetime_created'])) {
			$filter_datetime_created = $this->request->get['filter_datetime_created'];
		} else {
			$filter_datetime_created = '';
		}

		if (isset($this->request->get['filter_datetime_modified'])) {
			$filter_datetime_modified = $this->request->get['filter_datetime_modified'];
		} else {
			$filter_datetime_modified = '';
		}

		if (isset($this->request->get['filter_aws_cognito_id'])) {
			$filter_aws_cognito_id = $this->request->get['filter_aws_cognito_id'];
		} else {
			$filter_aws_cognito_id = 0;
		}

		if (isset($this->request->get['filter_consumer_status'])) {
			$filter_consumer_status = $this->request->get['filter_consumer_status'];
		} else {
			$filter_consumer_status = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'consumer_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_consumer_id'        => $filter_consumer_id,
			'filter_primary_mobile_number'	     => $filter_primary_mobile_number,
			'filter_consumer_id' => $filter_consumer_id,
			'filter_primary_payment_method_id'           => $filter_primary_payment_method_id,
			'filter_datetime_created'   => $filter_datetime_created,
			'filter_datetime_modified'   => $filter_datetime_modified,
			'filter_aws_cognito_id'   => $filter_aws_cognito_id,
			'filter_consumer_status'   => $filter_consumer_status,
			'sort'                   => $sort,
			'order'                  => $order,
			'start'                  => ($page - 1) * 50,
			'limit'                  => 50
			);

		$results = $this->model_consumer_consumer->getConsumers($filter_data);

		$this->json['data'] = json_encode($results);
		
		return $this->sendResponse();
	}


	public function add() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->getPost();

        	$this->loadData($post, self::$default_fields, self::$default_field_values);

	        $error = $this->validateForm($post);

	        if (empty($error)) {
				$this->json['data']['consumer_id'] = $this->model_consumer_consumer->addConsumer($post);
			} else {
	            $this->json['error'] = $error;
	            $this->statusCode = 400;
	        }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}
		
		return $this->sendResponse();
	}

	public function edit() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'PUT') {
			$post = $this->getPost();

        	if (isset($this->request->get['consumer_id']) && $this->request->get['consumer_id'] && !empty($post)) {
        		$consumer = $this->model_consumer_consumer->getConsumer($this->request->get['consumer_id']);

		        if($consumer) {
		            $this->loadData($post, self::$default_fields, self::$default_field_values, $consumer);

		            $error = $this->validateForm($post, $this->request->get['consumer_id']);

            		if (!empty($post) && empty($error)) {
						$this->model_consumer_consumer->editConsumer($this->request->get['consumer_id'], $post);
						$this->json['data'] = $this->model_consumer_consumer->getConsumer($this->request->get['consumer_id']);
					} else {
		                $this->json['error'] = $error;
		                $this->statusCode = 400;
		            }
				} else {
	                $this->json['error'][] = "Consumer not found";
	                $this->statusCode = 404;
	            }
			} else {
                $this->statusCode = 400;
                $this->json['error'][] = "No consumer ID or input data";
            }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("PUT");
		}

		return $this->sendResponse();
	}

	public function delete() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if($this->request->server['REQUEST_METHOD'] == 'DELETE') {
        	if(isset($this->request->get['consumer_id']) && $this->request->get['consumer_id']) {
        		$consumer = $this->model_consumer_consumer->getConsumer($this->request->get['consumer_id']);

		        if($consumer) {
					$this->model_consumer_consumer->deleteConsumerByConsumerId($this->request->get['consumer_id']);
				} else {
	                $this->json['error'] = "Consumer not found";
	                $this->statusCode = 404;
	            }
			} else {
				$this->json['error'][] = 'Error consumer ID';
                $this->statusCode = 400;
			}
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("DELETE");
		}

		return $this->sendResponse();
	}

	public function addOfferToConsumer() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->getPost();

	        if (isset(post['consumer_group_id']) && isset(post['consumer_id'])) {
				$this->json['data']['offer_to_consumer_id'] = $this->model_consumer_consumer->addOfferToConsumer($post['offer_id'], $post['consumer_id']);
			} else {
	            $this->json['error'] = $error;
	            $this->statusCode = 400;
	        }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}
		
		return $this->sendResponse();
	}

    private function validateForm($post, $consumer_id = null) {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

        $error = array();

        if (isset($post['primary_mobile_number']) && (utf8_strlen($post['primary_mobile_number']) < 7 || utf8_strlen($post['primary_mobile_number']) > 32)) {
            $error[] = 'Error mobile number';
        }

        if(isset($post['consumer_status']) && !in_array($post['consumer_status'], self::$default_consumer_status)) {
        	$error[] = 'Error consumer status';
        }

        return $error;
    }
}

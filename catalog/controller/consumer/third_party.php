<?php
class ControllerThirdPartyThirdParty extends Controller {
	static $default_fields = array(
		'consumer_id',
		'auth_token',
		'account_type',
		'mobile_number',
		'first_name',
		'last_name',
		'gender',
		'birthday',
		'payment_method',
		'address',
		'salary',
		'account_status',
    );

    static $default_field_values = array(
		'auth_token' => '',
		'account_type' => '',
		'mobile_number' => '',
		'first_name' => '',
		'last_name' => '',
		'gender' => '',
		'birthday' => '1900-01-01',
		'payment_method' => '',
		'address' => '',
		'salary' => '',
		'account_status' => 'active'
    );

    static $default_account_status = array(
    	'active', 
    	'disabled', 
    	'blacklisted', 
    	'delete'
    );

	public function index() {
        if ($this->request->server['REQUEST_METHOD'] === 'GET') {
            $this->get();
        } else if ($this->request->server['REQUEST_METHOD'] === 'POST') {
            $this->add();
        } else if ($this->request->server['REQUEST_METHOD'] === 'PUT') {
            $this->edit();
        } else if ($this->request->server['REQUEST_METHOD'] === 'DELETE') {
			$this->delete();
        }

        return $this->sendResponse();
    }

	public function get() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if (isset($this->request->get['filter_third_party_id'])) {
			$filter_third_party_id = $this->request->get['filter_third_party_id'];
		} else {
			$filter_third_party_id = '';
		}

		if (isset($this->request->get['filter_consumer_id'])) {
			$filter_consumer_id = $this->request->get['filter_consumer_id'];
		} else {
			$filter_consumer_id = '';
		}

		if (isset($this->request->get['filter_auth_token'])) {
			$filter_auth_token = $this->request->get['filter_auth_token'];
		} else {
			$filter_auth_token = '';
		}

		if (isset($this->request->get['filter_account_type'])) {
			$filter_account_type = $this->request->get['filter_account_type'];
		} else {
			$filter_account_type = '';
		}

		if (isset($this->request->get['filter_mobile_number'])) {
			$filter_mobile_number = $this->request->get['filter_mobile_number'];
		} else {
			$filter_mobile_number = '';
		}

		if (isset($this->request->get['filter_first_name'])) {
			$filter_first_name = $this->request->get['filter_first_name'];
		} else {
			$filter_first_name = '';
		}

		if (isset($this->request->get['filter_last_name'])) {
			$filter_last_name = $this->request->get['filter_last_name'];
		} else {
			$filter_last_name = '';
		}

		if (isset($this->request->get['filter_gender'])) {
			$filter_gender = $this->request->get['filter_gender'];
		} else {
			$filter_gender = '';
		}

		if (isset($this->request->get['filter_birthday'])) {
			$filter_birthday = $this->request->get['filter_birthday'];
		} else {
			$filter_birthday = '';
		}

		if (isset($this->request->get['filter_address'])) {
			$filter_address = $this->request->get['filter_address'];
		} else {
			$filter_address = '';
		}

		if (isset($this->request->get['filter_salary'])) {
			$filter_salary = $this->request->get['filter_salary'];
		} else {
			$filter_salary = 0;
		}

		if (isset($this->request->get['filter_datetime_created'])) {
			$filter_datetime_created = $this->request->get['filter_datetime_created'];
		} else {
			$filter_datetime_created = '';
		}

		if (isset($this->request->get['filter_datetime_modified'])) {
			$filter_datetime_modified = $this->request->get['filter_datetime_modified'];
		} else {
			$filter_datetime_modified = '';
		}

		if (isset($this->request->get['filter_account_status'])) {
			$filter_account_status = $this->request->get['filter_account_status'];
		} else {
			$filter_account_status = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'third_party_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_third_party_id'      => $filter_third_party_id,
			'filter_consumer_id'	     => $filter_consumer_id,
			'filter_auth_token'          => $filter_auth_token,
			'filter_account_type'        => $filter_account_type,
			'filter_mobile_number'       => $filter_mobile_number,
			'filter_first_name'          => $filter_first_name,
			'filter_last_name'           => $filter_last_name,
			'filter_gender'              => $filter_gender,
			'filter_birthday'            => $filter_birthday,
			'filter_address'             => $filter_address,
			'filter_salary'              => $filter_salary,
			'filter_datetime_modified'   => $filter_datetime_modified,
			'filter_aws_cognito_id'      => $filter_aws_cognito_id,
			'filter_account_status'      => $filter_account_status,
			'sort'                       => $sort,
			'order'                      => $order,
			'start'                      => ($page - 1) * 50,
			'limit'                      => 50
			);

		$results = $this->model_consumer_consumer->getThirdParties($filter_data);

		$this->json['data'] = json_encode($results);
		
		return $this->sendResponse();
	}

	public function add() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->getPost();

        	$this->loadData($post, self::$default_fields, self::$default_field_values);

	        $error = $this->validateForm($post);

	        if (empty($error)) {
				$this->json['data']['third_party_id'] = $this->model_consumer_consumer->addThirdParty($post);
			} else {
	            $this->json['error'] = $error;
	            $this->statusCode = 400;
	        }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}
		
		return $this->sendResponse();
	}

	public function edit() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'PUT') {
			$post = $this->getPost();

        	if (isset($this->request->get['third_party_id']) && ctype_digit($this->request->get['third_party_id']) && !empty($post)) {
        		$third_party = $this->model_consumer_consumer->getThirdParty($this->request->get['third_party_id']);

		        if($third_party) {
		            $this->loadData($post, self::$default_fields, self::$default_field_values, $third_party);

		            $error = $this->validateForm($post, $this->request->get['third_party_id']);

            		if (!empty($post) && empty($error)) {
						$this->model_consumer_consumer->editThirdPartyByThirdPartyId($this->request->get['third_party_id'], $post);
						$this->json['data'] = $this->model_consumer_consumer->getThirdParty($this->request->get['third_party_id']);
					} else {
		                $this->json['error'] = $error;
		                $this->statusCode = 400;
		            }
				} else {
	                $this->json['error'][] = "ThirdParty not found";
	                $this->statusCode = 404;
	            }
			} else {
                $this->statusCode = 400;
                $this->json['error'][] = "No ThirdParty ID or Input Data";
            }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("PUT");
		}

		return $this->sendResponse();
	}

	public function delete() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if($this->request->server['REQUEST_METHOD'] == 'DELETE') {
        	if(isset($this->request->get['third_party_id']) && ctype_digit($this->request->get['third_party_id'])) {
        		$third_party = $this->model_consumer_consumer->getThirdParty($this->request->get['third_party_id']);

		        if($third_party) {
					$this->model_consumer_consumer->deleteThirdParty($this->request->get['third_party_id']);
				} else {
	                $this->json['error'] = "ThirdParty not found";
	                $this->statusCode = 404;
	            }
			} else {
				$this->json['error'][] = 'Error third party ID';
                $this->statusCode = 400;
			}
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("DELETE");
		}

		return $this->sendResponse();
	}

    private function validateForm($post, $third_party_id = null) {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

        $error = array();

        if (empty($post['consumer_id'])) {
            $error[] = 'No consumer id';
        }

        if (!empty($post['first_name']) && utf8_strlen($post['first_name']) > 32) {
            $error[] = 'Error first name';
        }

        if (!empty($post['last_name']) && utf8_strlen($post['last_name']) > 32) {
            $error[] = 'Error last name';
        }

        if (!empty($post['birthday']) && (strtotime($post['birthday']) < strtotime('1900-01-01') || strtotime($post['birthday']) > strtotime('2010-01-01'))) {
            $error[] = 'Error birthday';
        }

        if (!empty($post['mobile_number']) && (utf8_strlen($post['mobile_number']) < 7 || utf8_strlen($post['mobile_number']) > 32)) {
            $error[] = 'Error mobile number';
        }

        if(isset($post['account_status']) && !in_array($post['account_status'], self::$default_account_status)) {
        	$error[] = 'Error account status';
        }

        return $error;
    }
}

<?php
class ControllerConsumerConsumerGroup extends Controller {
	static $default_fields = array(
		'consumer_group_name',
		'group_description',
		'group_status',
		'group_owner'
    );

    static $default_field_values = array(
		'group_description' => '',
		'group_status' => 'active',
		'group_owner' => 0
    );

    static $default_consumer_group_status = array(
    	'active', 
    	'disabled', 
    	'blacklisted', 
    	'delete'
    );

	public function index() {
        if ($this->request->server['REQUEST_METHOD'] === 'GET') {
            $this->get();
        } else if ($this->request->server['REQUEST_METHOD'] === 'POST') {
            $this->add();
        } else if ($this->request->server['REQUEST_METHOD'] === 'PUT') {
            $this->edit();
        } else if ($this->request->server['REQUEST_METHOD'] === 'DELETE') {
			$this->delete();
        }

        return $this->sendResponse();
    }

	public function get() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if (isset($this->request->get['filter_consumer_group_id'])) {
			$filter_consumer_group_id = $this->request->get['filter_consumer_group_id'];
		} else {
			$filter_consumer_group_id = '';
		}

		if (isset($this->request->get['filter_consumer_group_name'])) {
			$filter_consumer_group_name = $this->request->get['filter_consumer_group_name'];
		} else {
			$filter_consumer_group_name = '';
		}

		if (isset($this->request->get['filter_group_status'])) {
			$filter_group_status = $this->request->get['filter_group_status'];
		} else {
			$filter_group_status = '';
		}

		if (isset($this->request->get['filter_group_owner'])) {
			$filter_group_owner = $this->request->get['filter_group_owner'];
		} else {
			$filter_group_owner = '';
		}

		if (isset($this->request->get['filter_datetime_created'])) {
			$filter_datetime_created = $this->request->get['filter_datetime_created'];
		} else {
			$filter_datetime_created = '';
		}

		if (isset($this->request->get['filter_datetime_modified'])) {
			$filter_datetime_modified = $this->request->get['filter_datetime_modified'];
		} else {
			$filter_datetime_modified = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'consumer_group_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'filter_consumer_group_id'   => $filter_consumer_group_id,
			'filter_consumer_group_name' => $filter_consumer_group_name,
			'filter_group_status'        => $filter_group_status,
			'filter_group_owner'         => $filter_group_owner,
			'filter_datetime_created'    => $filter_datetime_created,
			'filter_datetime_modified'   => $filter_datetime_modified,
			'sort'                       => $sort,
			'order'                      => $order,
			'start'                      => ($page - 1) * 50,
			'limit'                      => 50
			);

		$results = $this->model_consumer_consumer->getConsumerGroups($filter_data);

		$this->json['data'] = json_encode($results);
		
		return $this->sendResponse();
	}


	public function add() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->getPost();

        	$this->loadData($post, self::$default_fields, self::$default_field_values);

	        $error = $this->validateForm($post);

	        if (empty($error)) {
				$this->json['data']['consumer_group_id'] = $this->model_consumer_consumer->addConsumerGroup($post);
			} else {
	            $this->json['error'] = $error;
	            $this->statusCode = 400;
	        }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}
		
		return $this->sendResponse();
	}

	public function edit() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'PUT') {
			$post = $this->getPost();

        	if (isset($this->request->get['consumer_group_id']) && ctype_digit($this->request->get['consumer_group_id']) && !empty($post)) {
        		$consumer_group = $this->model_consumer_consumer->getConsumerGroup($this->request->get['consumer_group_id']);

		        if($consumer_group) {
		            $this->loadData($post, self::$default_fields, self::$default_field_values, $consumer_group);

		            $error = $this->validateForm($post, $this->request->get['consumer_group_id']);

            		if (!empty($post) && empty($error)) {
						$this->model_consumer_consumer->editConsumerGroup($this->request->get['consumer_group_id'], $post);
						$this->json['data'] = $this->model_consumer_consumer->getConsumerGroup($this->request->get['consumer_group_id']);
					} else {
		                $this->json['error'] = $error;
		                $this->statusCode = 400;
		            }
				} else {
	                $this->json['error'][] = "Consumer group not found";
	                $this->statusCode = 404;
	            }
			} else {
                $this->statusCode = 400;
                $this->json['error'][] = "None consumer group ID or input data";
            }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("PUT");
		}

		return $this->sendResponse();
	}

	public function delete() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if($this->request->server['REQUEST_METHOD'] == 'DELETE') {
        	if(isset($this->request->get['consumer_group_id']) && ctype_digit($this->request->get['consumer_group_id'])) {
        		$consumer_group = $this->model_consumer_consumer->getConsumerGroup($this->request->get['consumer_group_id']);

		        if($consumer_group) {
					$this->model_consumer_consumer->deleteConsumerGroup($this->request->get['consumer_group_id']);
				} else {
	                $this->json['error'] = "Consumer group not found";
	                $this->statusCode = 404;
	            }
			} else {
				$this->json['error'][] = 'Error consumer group ID';
                $this->statusCode = 400;
			}
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("DELETE");
		}

		return $this->sendResponse();
	}

	public function addConsumerGroupToConsumer() {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->getPost();

	        if (isset(post['consumer_group_id']) && isset(post['consumer_id'])) {
				$this->json['data']['consumer_group_to_consumer_id'] = $this->model_consumer_consumer->addConsumerGroupToConsumer($post['consumer_group_id'], $post['consumer_id']);
			} else {
	            $this->json['error'] = $error;
	            $this->statusCode = 400;
	        }
		} else {
			$this->statusCode = 405;
            $this->allowedHeaders = array("POST");
		}
		
		return $this->sendResponse();
	}

    private function validateForm($post, $consumer_group_id = null) {
		$this->load->language('consumer/consumer');

		$this->load->model('consumer/consumer');

        $error = array();

        if (!empty($post['consumer_group_name']) || utf8_strlen($post['consumer_group_name']) < 2 || utf8_strlen($post['consumer_group_name']) > 32) {
            $error[] = 'Error consumer group name';
        }

        if(isset($post['group_status']) && !in_array($post['group_status'], self::$default_consumer_group_status)) {
        	$error[] = 'Error consumer group status';
        }

        return $error;
    }
}

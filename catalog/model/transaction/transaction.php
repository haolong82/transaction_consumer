<?php
class ModelTransactionTransaction extends Model {
	public function getTransaction($transaction_id) {
		$query = $this->db->query("SELECT * FROM os_transaction WHERE transaction_id = '" . $this->db->escape($transaction_id) . "'");
		return $query->row;
	}

	public function getTransactions($data = array()) {
		$sql = "SELECT * FROM os_transaction WHERE transaction_id != ''";
        
        $sort_data = array(
			'transaction_id',
            'reporting_partner_id',
            'reporting_consumer_id',
            'offer_id',
            'consumer_id',
            'offer_location_id',
            'datetime_reported',
            'datetime_created',
            'datetime_modified',
            'transaction_type',
			'version'
		);
        
        if (!empty($data['filter_transaction_id'])) {
			$sql .= " AND transaction_id = '" . (int)$data['filter_transaction_id'] . "'";
        }
        
        if (!empty($data['filter_reporting_partner_id'])) {
			$sql .= " AND reporting_partner_id = '" . $this->db->escape($data['filter_reporting_partner_id']) . "'";
        }

        if (!empty($data['filter_reporting_consumer_id'])) {
			$sql .= " AND reporting_consumer_id = '" . $this->db->escape($data['filter_reporting_consumer_id']) . "'";
        }
        
        if (!empty($data['filter_offer_id'])) {
			$sql .= " AND offer_id = '" . $this->db->escape($data['filter_offer_id']) . "'";
        }
        
        if (!empty($data['filter_consumer_id'])) {
			$sql .= " AND consumer_id = '" . $this->db->escape($data['filter_consumer_id']) . "'";
        }

        if (!empty($data['filter_offer_location_id'])) {
			$sql .= " AND offer_location_id = '" . $this->db->escape($data['filter_offer_location_id']) . "'";
        }
        
        if (!empty($data['filter_datetime_reported'])) {
			$sql .= " AND DATE(datetime_reported) = '" . $this->db->escape($data['filter_datetime_reported']) . "'";
        }

        if (!empty($data['filter_datetime_created'])) {
			$sql .= " AND DATE(datetime_created) = '" . $this->db->escape($data['filter_datetime_created']) . "'";
        }

        if (!empty($data['filter_transaction_type'])) {
			$sql .= " AND DATE(datetime_modified) = '" . $this->db->escape($data['filter_datetime_modified']) . "'";
        }

        if (!empty($data['filter_transaction_type'])) {
			$sql .= " AND transaction_type = '" . $this->db->escape($data['filter_transaction_type']) . "'";
        }

        if (!empty($data['filter_version'])) {
			$sql .= " AND version = '" . $this->db->escape($data['filter_version']) . "'";
        }

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY transaction_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 50;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function addTransaction($data = array()) {
		$this->db->query("INSERT INTO os_transaction SET transaction_id = UUID(), reporting_partner_id = '" . $this->db->escape($data['reporting_partner_id']) . "', reporting_consumer_id = '" . $this->db->escape($data['reporting_consumer_id']) . "', offer_id = '" . $this->db->escape($data['offer_id']) . "', consumer_id = '" . $this->db->escape($data['consumer_id']) . "', offer_location_id = '" . $this->db->escape($data['offer_location_id']) . "', datetime_reported = '" . $this->db->escape(date('Y-m-d H:i:s', strtotime($data['datetime_reported']))) . "', datetime_created = NOW() ,  transaction_type = '" . $this->db->escape($data['transaction_type']) . "', metrics = '" . $this->db->escape($data['metrics'] ? json_encode($data['metrics'], JSON_UNESCAPED_SLASHES) : '') . "', meta_data = '" . $this->db->escape($data['meta_data'] ? json_encode($data['meta_data'], JSON_UNESCAPED_SLASHES) : '') . "', version = '" . $this->db->escape($data['version']) . "'");

		$internal_transaction_id = $this->db->getLastId();

		$query = $this->db->query("SELECT transaction_id FROM os_transaction WHERE internal_transaction_id = '" . (int)$internal_transaction_id . "'");

		return $query->row['transaction_id'];
	}

	public function editTransaction($transaction_id, $data) {
		$this->db->query("UPDATE os_transaction SET reporting_partner_id = '" . $this->db->escape($data['reporting_partner_id']) . "', offer_id = '" . $this->db->escape($data['offer_id']) . "', consumer_id = '" . $this->db->escape($data['consumer_id']) . "', offer_location_id = '" . $this->db->escape($data['offer_location_id']) . "', datetime_reported = '" . $this->db->escape(date('Y-m-d H:i:s', strtotime($data['datetime_reported']))) . "', datetime_created = NOW() ,  transaction_type = '" . $this->db->escape($data['transaction_type']) . "', metrics = '" . $this->db->escape($data['metrics'] ? json_encode($data['metrics'], JSON_UNESCAPED_SLASHES) : '') . "', meta_data = '" . $this->db->escape($data['meta_data'] ? json_encode($data['meta_data'], JSON_UNESCAPED_SLASHES) : '') . "', version = '" . $this->db->escape($data['version']) . "' WHERE transaction_id = '" . (int)$transaction_id . "'");
	}

	public function deleteTransactionByTransactionId($transaction_id) {
		$this->db->query("DELETE FROM os_transaction WHERE transaction_id = '" . $this->db->escape($transaction_id) . "'");
	}

	public function getPartnerTransactionTotal($reporting_partner_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM os_transaction WHERE reporting_partner_id = '" . (int)$reporting_partner_id . "'");

		return $query->row['total'];
	}
}
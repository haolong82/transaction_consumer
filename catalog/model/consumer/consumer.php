<?php
class ModelConsumerConsumer extends Model {
	public function getConsumer($consumer_id) {
		$query = $this->db->query("SELECT * FROM os_consumer WHERE consumer_id = '" . $this->db->escape($consumer_id) . "'");

		return $query->row;
	}

	public function getConsumers($data = array()) {
		$sql = "SELECT * FROM os_consumer WHERE consumer_id != ''";
        
        $sort_data = array(
			'consumer_id',
			'primary_mobile_number',
			'primary_payment_method_id',
			'datetime_created',
			'datetime_modified',
			'aws_cognito_id',
			'consumer_status'
		);
        
        if (!empty($data['filter_consumer_id'])) {
			$sql .= " AND consumer_id = '" . $this->db->escape($data['filter_consumer_id']) . "'";
        }
        
        if (!empty($data['filter_primary_mobile_number'])) {
			$sql .= " AND primary_mobile_number = '" . $this->db->escape($data['filter_primary_mobile_number']) . "'";
        }
        
        if (!empty($data['filter_primary_payment_method_id'])) {
			$sql .= " AND primary_payment_method_id = '" . (int)$data['filter_primary_payment_method_id'] . "'";
        }

        if (!empty($data['filter_datetime_created'])) {
			$sql .= " AND DATE(datetime_created) = '" . $this->db->escape($data['filter_datetime_created']) . "'";
        }
        
        if (!empty($data['filter_datetime_modified'])) {
			$sql .= " AND DATE(datetime_modified) = '" . $this->db->escape($data['filter_datetime_modified']) . "'";
        }

        if (!empty($data['filter_aws_cognito_id'])) {
			$sql .= " AND aws_cognito_id = '" . $this->db->escape($data['filter_aws_cognito_id']) . "'";
        }

        if (!empty($data['filter_consumer_status'])) {
			$sql .= " AND consumer_status = '" . $this->db->escape($data['filter_consumer_status']) . "'";
        }

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY consumer_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 50;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function addConsumer($data = array()) {
		$this->db->query("INSERT INTO os_consumer SET consumer_id = UUID(), reporting_partner_id = '" . $this->db->escape($data['reporting_partner_id']) . "', reporting_consumer_id = '" . $this->db->escape($data['reporting_consumer_id']) . "', primary_mobile_number = '" . $this->db->escape($data['mobile_number']) . "', primary_payment_method_id = '" . (int)$data['primary_payment_method_id'] . "', aws_cognito_id = '" . $this->db->escape($data['aws_cognito_id']) . "', datetime_created = NOW() , consumer_status = '" . $this->db->escape($data['consumer_status']) . "'");

		$internal_consumer_id = $this->db->getLastId();

		$query = $this->db->query("SELECT consumer_id FROM os_consumer WHERE internal_consumer_id = '" . (int)$internal_consumer_id . "'");

		return $query->row['consumer_id'];
	}

	public function editConsumer($consumer_id, $data) {
		$this->db->query("UPDATE os_consumer SET reporting_partner_id = '" . $this->db->escape($data['reporting_partner_id']) . "', reporting_consumer_id = '" . $this->db->escape($data['reporting_consumer_id']) . "', primary_mobile_number = '" . $this->db->escape($data['mobile_number']) . "', primary_payment_method_id = '" . (int)$data['primary_payment_method_id'] . "', aws_cognito_id = '" . $this->db->escape($data['aws_cognito_id']) . "', datetime_modified = NOW() , consumer_status = '" . $this->db->escape($data['consumer_status']) . "' WHERE consumer_id = '" . $this->db->escape($consumer_id) . "'");
	}

	public function deleteConsumerByConsumerId($reporting_partner_id) {
		$this->db->query("DELETE FROM os_consumer WHERE consumer_id = '" . $this->db->escape($consumer_id) . "'");
	}

	public function existConsumer($consumer_data) {
		$query = $this->db->query("SELECT consumer_id FROM `os_consumer` WHERE reporting_partner_id = '". $this->db->escape($consumer_data['reporting_partner_id']) ."' AND reporting_consumer_id = '" . $this->db->escape($consumer_data['reporting_consumer_id']) . "'");

		return $query->row['consumer_id'];
	}

	public function getConsumerTotal() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM os_consumer");

		return $query->row['total'];
	}

	public function getConsumerGroup($consumer_group_id) {
		$query = $this->db->query("SELECT * FROM os_consumer_group WHERE consumer_group_id = '" . (int)$consumer_group_id . "'");

		return $query->row;
	}

	public function getConsumerGroups($data = array()) {
		$sql = "SELECT * FROM os_consumer_group WHERE consumer_group_id > 0";
        
        $sort_data = array(
        	'consumer_group_id',
			'consumer_group_name',
			'group_description',
			'group_status',
			'group_owner',
			'datetime_created',
			'datetime_modified'
		);
        
        if (!empty($data['filter_consumer_group_id'])) {
			$sql .= " AND consumer_group_id = '" . (int)$data['filter_consumer_group_id'] . "'";
        }
        
        if (!empty($data['filter_consumer_group_name'])) {
			$sql .= " AND consumer_group_name LIKE '%" . $this->db->escape($data['filter_consumer_group_name']) . "%'";
        }
        
        if (!empty($data['filter_group_description'])) {
			$sql .= " AND group_description LIKE '%" . $this->db->escape($data['filter_group_description']) . "%'";
        }

        if (!empty($data['filter_group_status'])) {
			$sql .= " AND group_status = '" . $this->db->escape($data['group_status']) . "'";
        }

        if (!empty($data['filter_group_owner'])) {
			$sql .= " AND group_owner = '" . (int)$data['filter_group_owner'] . "'";
        }
        
        if (!empty($data['filter_datetime_created'])) {
			$sql .= " AND DATE(datetime_created) = '" . $this->db->escape($data['filter_datetime_created']) . "'";
        }
        
        if (!empty($data['filter_datetime_modified'])) {
			$sql .= " AND DATE(datetime_modified) = '" . $this->db->escape($data['filter_datetime_modified']) . "'";
        }

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY consumer_group_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 50;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function addConsumerGroup($data = array()) {
		$this->db->query("INSERT INTO os_consumer_group SET consumer_group_name = '" . $this->db->escape($data['consumer_group_name']) . "', group_description = '" . $this->db->escape($data['group_description']) . "', datetime_created = NOW() , group_status = '" . $this->db->escape($data['group_status']) . "', group_owner = '" . (int)$data['group_owner'] . "'");

		return $this->db->getLastId();
	}

	public function editConsumerGroup($consumer_group_id, $data) {
		$this->db->query("UPDATE os_consumer_group SET consumer_group_name = '" . $this->db->escape($data['consumer_group_name']) . "', group_description = '" . $this->db->escape($data['group_description']) . "', datetime_created = NOW() , group_status = '" . $this->db->escape($data['group_status']) . "', group_owner = '" . (int)$data['group_owner'] . "' WHERE consumer_group_id = '" . (int)$consumer_group_id . "'");
	}

	public function deleteConsumerGroup($consumer_group_id) {
		$this->db->query("DELETE FROM os_consumer_group WHERE consumer_group_id = '" . (int)$consumer_group_id . "'");
		$this->db->query("DELETE FROM os_consumer_group_to_consumer WHERE consumer_group_id = '" . (int)$consumer_group_id . "'");
	}

	public function addConsumerGroupToConsumer($consumer_group_id, $internal_consumer_id) {
		$this->db->query("INSERT INTO os_consumer_group_to_consumer SET internal_consumer_id = '" . (int)$data['internal_consumer_id'] . "', consumer_group_id = '" . (int)$data['consumer_group_id'] . "', datetime_created = NOW()");

		return $this->db->getLastId();
	}

	public function deleteConsumerGroupToConsumer($internal_consumer_id) {
		$this->db->query("DELETE FROM os_consumer_group_to_consumer WHERE internal_consumer_id = '" . (int)$internal_consumer_id . "'");
	}

	public function addOfferToConsumer($offer_id, $internal_consumer_id, $visibility_status = '', $redemption_status = '') {
		$this->db->query("INSERT INTO os_offer_to_consumer SET internal_consumer_id = '" . (int)$data['internal_consumer_id'] . "', offer_id = '" . (int)$data['offer_id'] . "', datetime_created = NOW(), visibility_status = '" . $this->db->escape($data['visibility_status']) . ", redemption_status = '" . $this->db->escape($data['redemption_status']) . "'");

		return $this->db->getLastId();
	}

	public function deleteConsumerToOfferByConsumerId($internel_consumer_id) {
		$this->db->query("DELETE FROM os_offer_to_consumer WHERE internel_consumer_id = '" . (int)$internel_consumer_id . "'");
	}

	public function deleteConsumerToOfferByOfferId($offer_id) {
		$this->db->query("DELETE FROM os_offer_to_consumer WHERE offer_id = '" . (int)$offer_id . "'");
	}

	public function getPaymentMethod($payment_method_id) {
		$query = $this->db->query("SELECT * FROM os_payment_method WHERE payment_method_id = '" . (int)$payment_method_id . "'");

		return $query->row;
	}

	public function getPaymentMethods($data = array()) {
		$sql = "SELECT * FROM os_payment_method WHERE payment_method_id > 0";
        
        $sort_data = array(
        	'payment_method_id',
			'payment_service_name',
			'service_user_identifier',
			'meta_data',
			'status',
			'datetime_created',
			'datetime_modified'
		);
        
        if (!empty($data['filter_payment_method_id'])) {
			$sql .= " AND payment_method_id = '" . (int)$data['filter_payment_method_id'] . "'";
        }
        
        if (!empty($data['filter_payment_service_name'])) {
			$sql .= " AND payment_service_name LIKE '%" . $this->db->escape($data['filter_payment_service_name']) . "%'";
        }
        
        if (!empty($data['filter_grservice_user_identifier'])) {
			$sql .= " AND service_user_identifier LIKE '%" . $this->db->escape($data['filter_service_user_identifier']) . "%'";
        }

        if (!empty($data['filter_meta_data'])) {
			$sql .= " AND meta_data LIKE '%" . $this->db->escape($data['filter_meta_data']) . "%'";
        }

        if (!empty($data['filter_status'])) {
			$sql .= " AND status = '" . $this->db->escape($data['filter_status']) . "'";
        }
        
        if (!empty($data['filter_datetime_created'])) {
			$sql .= " AND DATE(datetime_created) = '" . $this->db->escape($data['filter_datetime_created']) . "'";
        }
        
        if (!empty($data['filter_datetime_modified'])) {
			$sql .= " AND DATE(datetime_modified) = '" . $this->db->escape($data['filter_datetime_modified']) . "'";
        }

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY payment_method_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 50;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function addPaymentMethod($data = array()) {
		if(!isset($data['service_user_identifier']))
			$data['service_user_identifier'] = '';

		if(!isset($data['meta_data']))
			$data['meta_data'] = '';

		$this->db->query("INSERT INTO os_payment_method SET payment_service_name = '" . $this->db->escape($data['payment_service_name']) . "', service_user_identifier = '" . $this->db->escape($data['service_user_identifier']) . "', meta_data = '" . $this->db->escape($data['meta_data']) . ", status = '" . $this->db->escape($data['status']) . "', datetime_created = NOW()");

		return $this->db->getLastId();
	}

	public function editPaymentMethod($data) {
		$this->db->query("UPDATE os_payment_method SET payment_service_name = '" . $this->db->escape($data['payment_service_name']) . "', service_user_identifier = '" . $this->db->escape($data['service_user_identifier']) . "', meta_data = '" . $this->db->escape($data['meta_data']) . ", status = '" . $this->db->escape($data['status']) . "', datetime_modified = NOW() WHERE payment_method_id = '" . (int)$payment_method_id . "'");
	}

	public function deletePaymentMethod($payment_method_id) {
		$this->db->query("DELETE FROM os_payment_method WHERE payment_method_id = '" . (int)$payment_method_id . "'");
	}

	public function getThirdParty($third_party_id) {
		$query = $this->db->query("SELECT * FROM os_third_party WHERE third_party_id = '" . (int)$third_party_id . "'");

		return $query->row;
	}

	public function getThirdParties($data = array()) {
		$sql = "SELECT * FROM os_third_party WHERE third_party_id > 0";
        
        $sort_data = array(
        	'third_party_id',
        	'consumer_id',
			'auth_token',
			'account_type',
			'mobile_number',
			'first_name',
			'last_name',
			'gender',
			'birthday',
			'address',
			'salary',
			'account_status',
		);
        
        if (!empty($data['filter_consumer_id'])) {
			$sql .= " AND consumer_id = '" . (int)$data['filter_consumer_id'] . "'";
        }
        
        if (!empty($data['filter_auth_token'])) {
			$sql .= " AND auth_token = '" . $this->db->escape($data['filter_auth_token']) . "'";
        }
        
        if (!empty($data['filter_account_type'])) {
			$sql .= " AND account_type = '" . $this->db->escape($data['filter_account_type']) . "'";
        }

        if (!empty($data['filter_mobile_number'])) {
			$sql .= " AND mobile_number = '" . $this->db->escape($data['filter_mobile_number']) . "'";
        }

        if (!empty($data['filter_first_name'])) {
			$sql .= " AND first_name LIKE '%" . $this->db->escape($data['filter_first_name']) . "%'";
        }

        if (!empty($data['filter_last_name'])) {
			$sql .= " AND last_name LIKE '%" . $this->db->escape($data['filter_last_name']) . "%'";
        }

        if (!empty($data['filter_birthday'])) {
			$sql .= " AND birthday = '" . $this->db->escape($data['filter_birthday']) . "'";
        }

        if (!empty($data['filter_address'])) {
			$sql .= " AND address LIKE '%" . $this->db->escape($data['filter_address']) . "%'";
        }

        if (!empty($data['filter_salary'])) {
			$sql .= " AND salary > '" . $this->db->escape($data['filter_salary']) . "'";
        }

        if (!empty($data['filter_account_status'])) {
			$sql .= " AND account_status = '" . $this->db->escape($data['filter_account_status']) . "'";
        }
        
        if (!empty($data['filter_datetime_created'])) {
			$sql .= " AND DATE(datetime_created) = '" . $this->db->escape($data['filter_datetime_created']) . "'";
        }
        
        if (!empty($data['filter_datetime_modified'])) {
			$sql .= " AND DATE(datetime_modified) = '" . $this->db->escape($data['filter_datetime_modified']) . "'";
        }

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY third_party_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 50;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function addThirdParty($data = array()) {
		$this->db->query("INSERT INTO os_third_party SET consumer_id = '" . $this->db->escape($data['consumer_id']) . "', auth_token = '" . $this->db->escape($data['auth_token']) . "', account_type = '" . $this->db->escape($data['account_type']) . "', mobile_number = '" . $this->db->escape($data['mobile_number']) . "', first_name = '" . $this->db->escape($data['first_name']) . "', last_name = '" . $this->db->escape($data['last_name']) . "', gender = '" . $this->db->escape($data['gender']) . "', birthday = '" . $this->db->escape(date('Y-m-d', strtotime($data['birthday']))) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', address = '" . $this->db->escape($data['address']) . "', salary = '" . $this->db->escape($data['salary']) . "', account_status = '" . $this->db->escape($data['account_status']) . "', datetime_created = NOW()");

		return $this->db->getLastId();
	}

	public function editThirdParty($consumer_id, $data) {
		$this->db->query("UPDATE os_third_party SET auth_token = '" . $this->db->escape($data['auth_token']) . "', account_type = '" . $this->db->escape($data['account_type']) . "', mobile_number = '" . $this->db->escape($data['mobile_number']) . "', first_name = '" . $this->db->escape($data['first_name']) . "', last_name = '" . $this->db->escape($data['last_name']) . "', gender = '" . $this->db->escape($data['gender']) . "', birthday = '" . $this->db->escape(date('Y-m-d', strtotime($data['birthday']))) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', address = '" . $this->db->escape($data['address']) . "', salary = '" . $this->db->escape($data['salary']) . "', account_status = '" . $this->db->escape($data['account_status']) . "', datetime_modified = NOW() WHERE consumer_id = '" . $this->db->escape($consumer_id) . "'");
	}

	public function editThirdPartyByThirdPartyId($third_party_id, $data) {
		$this->db->query("UPDATE os_third_party SET consumer_id = '" . $this->db->escape($data['consumer_id']) . "', auth_token = '" . $this->db->escape($data['auth_token']) . "', account_type = '" . $this->db->escape($data['account_type']) . ", mobile_number = '" . $this->db->escape($data['mobile_number']) . ", first_name = '" . $this->db->escape($data['first_name']) . ", last_name = '" . $this->db->escape($data['last_name']) . ", gender = '" . $this->db->escape($data['gender']) . ", birthday = '" . $this->db->escape(date('Y-m-d', strtotime($data['birthday']))) . "', payment_method = '" . $this->db->escape($data['payment_method']) . ", address = '" . $this->db->escape($data['address']) . "', salary = '" . $this->db->escape($data['salary']) . ", account_status = '" . $this->db->escape($data['account_status']) . "', datetime_modified = NOW() WHERE third_party_id = '" . (int)$third_party_id . "'");
	}


	public function deleteThirdParty($third_party_id) {
		$this->db->query("DELETE FROM os_third_party WHERE third_party_id = '" . (int)$third_party_id . "'");
	}
}
<?php
// HTTP
define('HTTP_SERVER', 'http://52.8.91.52/');

// HTTPS
define('HTTPS_SERVER', 'http://52.8.91.52/');

// DIR
define('DIR_APPLICATION', '/var/www/transaction_consumer/catalog/');
define('DIR_SYSTEM', '/var/www/transaction_consumer/system/');
define('DIR_IMAGE', '/var/www/transaction_consumer/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'transaction-consumer.cbhh6qzsa0mk.us-west-1.rds.amazonaws.com');
define('DB_USERNAME', 'youworlduser');
define('DB_PASSWORD', 'youworld2017');
define('DB_DATABASE', 'transaction_consumer');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');